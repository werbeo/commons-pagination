# werbeo - commons-pagination

[![Maven Central](https://img.shields.io/maven-central/v/org.infinitenature/commons-pagination-parent.svg)]() 

An api for pagination requests and respsonses. Inspired by spring-data pagination.

## Usage

### Add repository

```
<repositories>

    <!-- Other repositories -->
    
    <repository>
	    <snapshots>
		    <enabled>false</enabled>
        </snapshots>
	    <id>artifactory-loe</id>
	    <name>libs-release</name>
	    <url>https://artifactory.loe.auf.uni-rostock.de/artifactory/libs-release/</url>
    </repository>
</repositories>
```

### Add dependency

```
<dependencies>
	
   <!-- Other dependencies -->
	
	<dependency>
		<groupId>org.infinitenature</groupId>
		<artifactId>commons-pagination</artifactId>
		<version>0.0.1</version>
	</dependency>
</dependencies>
```

### Use in API

Use `OffsetRequest` and `Slice`:

```
	public Slice<String> findStrings(String contains, OffsetRequest pageRequest);
```

Or `PageRequest` and `Page`:

```
	public Page<String> findStrings(String contains, PageReqeust pageRequest);
```

### Class hierarchy

A `Page` extends a `Slice` and a `PageRequest` extends an `OffsetRequest`.
	