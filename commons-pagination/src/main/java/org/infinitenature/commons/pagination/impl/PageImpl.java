package org.infinitenature.commons.pagination.impl;

import java.util.List;
import java.util.Optional;

import org.infinitenature.commons.pagination.Page;
import org.infinitenature.commons.pagination.PageRequest;

import net.vergien.beanautoutils.annotation.Bean;

/**
 * Represents a page of entities
 *
 * @author dve
 *
 * @param <T>
 *           type of the entities of the page
 * @param <S>
 *           type of the sort field
 */
@Bean
public class PageImpl<T, S> extends SliceImpl<T, S> implements Page<T, S>
{
   private final long total;
   private final PageRequest<S> request;

   public PageImpl(List<T> content, PageRequest<S> request, long total)
   {
      super(content, request);
      this.total = total;
      this.request = request;
   }

   @Override
   public int getTotalPages()
   {
      return getSize() == 0 ? 1
            : (int) Math.ceil((double) total / (double) getSize());
   }

   @Override
   public long getTotalElements()
   {
      return total;
   }

   @Override
   public int getNumber()
   {
      return request.getPage();
   }

   @Override
   public boolean isFirst()
   {
      return !hasPrevious();
   }

   @Override
   public boolean isLast()
   {
      return !hasNext();
   }

   @Override
   public boolean hasNext()
   {
      return getNumber() < getTotalPages();
   }

   @Override
   public boolean hasPrevious()
   {
      return getNumber() > 1;
   }

   @Override
   public Optional<PageRequest<S>> nextPageRequest()
   {
      if (hasNext())
      {
         return Optional.of(request.next());
      } else
      {
         return Optional.empty();
      }
   }

   @Override
   public Optional<PageRequest<S>> previousPageRequest()
   {
      if (hasPrevious())
      {
         return Optional.of(request.previous());
      } else
      {
         return Optional.empty();
      }
   }

   protected long getTotal()
   {
      return total;
   }

   @Override
   public String toString()
   {
      return PageImplBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PageImplBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PageImplBeanUtil.doEquals(this, obj);
   }
}
