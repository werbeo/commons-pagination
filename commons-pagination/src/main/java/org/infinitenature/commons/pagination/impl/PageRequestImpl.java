package org.infinitenature.commons.pagination.impl;

import org.infinitenature.commons.pagination.PageRequest;
import org.infinitenature.commons.pagination.SortOrder;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class PageRequestImpl<T> extends OffsetRequestImpl<T>
      implements PageRequest<T>
{
   private final int page;
   private final int pageSize;

   /**
    *
    * @param page
    *           One based index of the requested PageImpl
    * @param pageSize
    * @param sortField
    * @param sortOrder
    */
   public PageRequestImpl(int page, int pageSize, T sortField,
         SortOrder sortOrder)
   {
      super((page - 1) * pageSize, pageSize, sortOrder, sortField);
      this.page = page;
      this.pageSize = pageSize;
   }

   public PageRequestImpl(int page, int pageSize)
   {
      this(page, pageSize, null, null);
   }

   /*
    * (non-Javadoc)
    *
    * @see org.infinitenature.commons.pagination.impl.PageRequest#getPage()
    */
   @Override
   public int getPage()
   {
      return page;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.infinitenature.commons.pagination.impl.PageRequest#getPageSize()
    */
   @Override
   public int getPageSize()
   {
      return pageSize;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.infinitenature.commons.pagination.impl.PageRequest#previous()
    */
   @Override
   public PageRequest<T> previous()
   {
      return getPage() == 1 ? this
            : new PageRequestImpl<>(getPage() - 1, getPageSize(),
                  getSortField(),
                  getSortOrder());
   }

   /*
    * (non-Javadoc)
    *
    * @see org.infinitenature.commons.pagination.impl.PageRequest#next()
    */
   @Override
   public PageRequest<T> next()
   {
      return new PageRequestImpl<>(getPage() + 1, getPageSize(),
            getSortField(),
            getSortOrder());
   }

   @Override
   public int hashCode()
   {
      return PageRequestImplBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PageRequestImplBeanUtil.doEquals(this, obj);
   }

   @Override
   public String toString()
   {
      return PageRequestImplBeanUtil.doToString(this);
   }

}
