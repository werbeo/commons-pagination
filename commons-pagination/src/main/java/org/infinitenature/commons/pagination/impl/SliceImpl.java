package org.infinitenature.commons.pagination.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;

import net.vergien.beanautoutils.annotation.Bean;


/**
 * Represents a slice of entities
 *
 * @author dve
 *
 * @param <T>
 *           type of the entities of the page
 * @param <S>
 *           type of the sort field
 */
@Bean
public class SliceImpl<T, S> implements Slice<T, S>
{
   private List<T> content = new ArrayList<>();
   private final OffsetRequest<S> request;

   public SliceImpl(List<T> content, OffsetRequest<S> request)
   {
      super();
      this.content = content;
      this.request = request;
   }

   @Override
   public Iterator<T> iterator()
   {
      return getContent().iterator();
   }

   @Override
   public List<T> getContent()
   {
      return Collections.unmodifiableList(content);
   }

   @Override
   public boolean hasContent()
   {
      return !content.isEmpty();
   }

   @Override
   public int getSize()
   {
      return request.getCount();
   }

   @Override
   public int getNumberOfElements()
   {
      return content.size();
   }

   @Override
   public Optional<SortOrder> getSortOrder()
   {
      return Optional.ofNullable(request.getSortOrder());
   }

   @Override
   public Optional<S> getSortField()
   {
      return Optional.ofNullable(request.getSortField());
   }

   protected OffsetRequest<S> getRequest()
   {
      return request;
   }

   @Override
   public String toString()
   {
      return SliceImplBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SliceImplBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SliceImplBeanUtil.doEquals(this, obj);
   }
}
