package org.infinitenature.commons.pagination.impl;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OffsetRequestImpl<T> implements OffsetRequest<T>
{
   public static final OffsetRequestImpl<?> UNLIMITED_RESULTS = new OffsetRequestImpl<>(
         0, 0);

   private final int offset;
   private final int count;
   private final SortOrder sortOrder;
   private final T sortField;

   public OffsetRequestImpl(int offset, int count, SortOrder sortOrder,
         T sortField)
   {
      super();
      this.offset = offset;
      this.count = count;
      this.sortOrder = sortOrder;
      this.sortField = sortField;
   }

   public OffsetRequestImpl(int offset, int count)
   {
      this(offset, count, null, null);
   }

   @Override
   public int getOffset()
   {
      return offset;
   }

   @Override
   public int getCount()
   {
      return count;
   }

   @Override
   public SortOrder getSortOrder()
   {
      return sortOrder;
   }

   @Override
   public T getSortField()
   {
      return sortField;
   }

   @Override
   public int hashCode()
   {
      return OffsetRequestImplBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OffsetRequestImplBeanUtil.doEquals(this, obj);
   }

   @Override
   public String toString()
   {
      return OffsetRequestImplBeanUtil.doToString(this);
   }

}
