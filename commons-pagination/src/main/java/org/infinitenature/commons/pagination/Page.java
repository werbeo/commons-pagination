package org.infinitenature.commons.pagination;

import java.util.Optional;

/**
 * Represents a page of entities
 *
 * @author dve
 *
 * @param <T>
 *           type of the entities of the page
 * @param <S>
 *           type of the sort field
 */
public interface Page<T, S> extends Slice<T, S>
{
   /**
    * Returns the number of total pages.
    *
    * @return the number of total pages
    */
   int getTotalPages();

   /**
    * Returns the total amount of elements.
    *
    * @return the total amount of elements
    */
   long getTotalElements();

   /**
    * Returns the number of the current {@link Page}. Is always non-negative.
    *
    * @return the number of the current {@link Page}.
    */
   int getNumber();

   /**
    * Returns whether the current {@link Page} is the first one.
    *
    * @return
    */
   boolean isFirst();

   /**
    * Returns whether the current {@link Page} is the last one.
    *
    * @return
    */
   boolean isLast();

   /**
    * Returns if there is a next {@link Page}.
    *
    * @return if there is a next {@link Page}.
    */
   boolean hasNext();

   /**
    * Returns if there is a previous {@link Page}.
    *
    * @return if there is a previous {@link Page}.
    */
   boolean hasPrevious();

   Optional<PageRequest<S>> nextPageRequest();

   Optional<PageRequest<S>> previousPageRequest();

}
