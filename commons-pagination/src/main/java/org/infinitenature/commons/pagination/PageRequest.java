package org.infinitenature.commons.pagination;

/**
 *
 * @param <S>
 *           the type of the sort field
 */
public interface PageRequest<S> extends OffsetRequest<S>
{

   /**
    * Returns the one based index of the requested PageImpl
    */
   int getPage();

   int getPageSize();

   /**
    * Return the {@link PageRequest} for the page which is the predcessor of the
    * page returend with this request.
    *
    * @return the request for the predcessor page, this if this request is for
    *         the first page
    */
   PageRequest<S> previous();

   PageRequest<S> next();

}