package org.infinitenature.commons.pagination;

public enum SortOrder
{
   ASC, DESC
}
