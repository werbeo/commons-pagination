package org.infinitenature.commons.pagination;

import java.util.List;
import java.util.Optional;

/**
 * Represents a slice of entities
 *
 * @author dve
 *
 * @param <T>
 *           type of the entities of the page
 * @param <S>
 *           type of the sort field
 */
public interface Slice<T, S> extends Iterable<T>
{
   /**
    * Returns the page content as unmodifiable {@link List}.
    *
    * @return
    */
   List<T> getContent();

   /**
    * Returns whether the {@link Slice} has content at all.
    *
    * @return
    */
   boolean hasContent();

   /**
    * Returns the size of the {@link Slice}.
    *
    * @return the size of the {@link Slice}.
    */
   int getSize();

   /**
    * Returns the number of elements currently on this {@link Slice}.
    *
    * @return the number of elements currently on this {@link Slice}.
    */
   int getNumberOfElements();

   /**
    * Returns the sort order of this {@link Slice}.
    *
    * @return the sort order of this {@link Slice}.
    */
   Optional<SortOrder> getSortOrder();

   /**
    * Returns the sort field of this {@link Slice}.
    *
    * @return the sort field of this {@link Slice}.
    */
   Optional<S> getSortField();
}
