package org.infinitenature.commons.pagination;

/**
 *
 * @param <S>
 *           the type of the sort field
 */
public interface OffsetRequest<S>
{
   int getOffset();

   int getCount();

   SortOrder getSortOrder();

   S getSortField();
}
