package org.infinitenature.commons.pagination.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.infinitenature.commons.pagination.PageRequest;
import org.junit.Test;

public class TestPageRequestImpl
{

   @Test
   public void test_next()
   {
      PageRequest<String> pageRequest = new PageRequestImpl<>(2, 20);

      PageRequest<String> next = pageRequest.next();

      assertThat(next.getPageSize(), is(20));
      assertThat(next.getPage(), is(3));
      assertThat(next.getOffset(), is(40));
   }

   @Test
   public void test_previous_isFirstPage()
   {
      PageRequest<String> pageRequest = new PageRequestImpl<>(1, 20);

      PageRequest<String> previous = pageRequest.previous();

      assertThat(previous, is(pageRequest));
   }

   @Test
   public void test_previous()
   {
      PageRequest<String> pageRequest = new PageRequestImpl<>(2, 20);

      PageRequest<String> previous = pageRequest.previous();

      assertThat(previous.getPage(), is(1));
      assertThat(previous.getOffset(), is(0));
   }

}
