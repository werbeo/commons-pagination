package org.infinitenature.commons.pagination.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import org.infinitenature.commons.pagination.SortOrder;
import org.junit.Test;

public class TestOffsetRequestImpl
{
   private OffsetRequestImpl<String> request = new OffsetRequestImpl<>(1, 10);
   private OffsetRequestImpl<String> request_equal = new OffsetRequestImpl<>(1,
         10);
   private OffsetRequestImpl<String> request_otherOffset = new OffsetRequestImpl<>(
         2, 10);
   private OffsetRequestImpl<String> request_otherCount = new OffsetRequestImpl<>(
         1, 20);

   private OffsetRequestImpl<String> request_withSort = new OffsetRequestImpl<>(
         1, 10,
	 SortOrder.ASC, "bla");

   private OffsetRequestImpl<String> request_withOtherSortOrder = new OffsetRequestImpl<>(
	 1, 10, SortOrder.DESC, "bla");

   private OffsetRequestImpl<String> request_withOtherSortField = new OffsetRequestImpl<>(
	 1, 10, SortOrder.ASC, "blub");

   @Test
   public void testHashCode()
   {
      assertThat(request.hashCode(), is(request_equal.hashCode()));

      assertThat(request.hashCode(), is(not(request_otherOffset.hashCode())));

      assertThat(request.hashCode(), is(not(request_withSort.hashCode())));
   }

   @Test
   public void testSortOrder_String()
   {

   }

   @Test
   public void testEquals()
   {

      assertThat(request, is(request));

      assertThat(request, is(request_equal));

      assertThat(request, is(not("Hello, World!")));

      assertThat(request.equals(null), is(false));

      assertThat(request, is(not(request_otherOffset)));

      assertThat(request, is(not(request_otherCount)));

      assertThat(request, is(not(request_withSort)));

      assertThat(request_withSort, is(not(request_withOtherSortOrder)));

      assertThat(request_withSort, is(not(request_withOtherSortField)));
   }

}
