package org.infinitenature.commons.pagination.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;

import org.infinitenature.commons.pagination.Page;
import org.infinitenature.commons.pagination.PageRequest;
import org.junit.Before;
import org.junit.Test;

public class TestPageImpl
{

   private Page<String, String> page1;
   private Page<String, String> page2;
   private PageRequest<String> request1;
   private PageRequest<String> request2;

   @Before
   public void setUp()
   {
      request1 = new PageRequestImpl<>(1, 3);
      page1 = new PageImpl<>(Arrays.asList("A", "B", "C"), request1, 5);

      request2 = new PageRequestImpl<>(2, 3);
      page2 = new PageImpl<>(Arrays.asList("D", "E"), request2, 5);
   }

   @Test
   public void testGetTotalPages()
   {
      assertThat(page1.getTotalPages(), is(2));
   }

   @Test
   public void testGetTotalElements()
   {
      assertThat(page1.getTotalElements(), is(5L));
   }

   @Test
   public void testGetNumber()
   {
      assertThat(page1.getNumber(), is(1));
      assertThat(page2.getNumber(), is(2));
   }

   @Test
   public void testIsFirst()
   {
      assertThat(page1.isFirst(), is(true));
      assertThat(page2.isFirst(), is(false));
   }

   @Test
   public void testIsLast()
   {
      assertThat(page1.isLast(), is(false));
      assertThat(page2.isLast(), is(true));
   }

   @Test
   public void testHasNext()
   {
      assertThat(page1.hasNext(), is(true));
      assertThat(page2.hasNext(), is(false));
   }

   @Test
   public void testHasPrevious()
   {
      assertThat(page1.hasPrevious(), is(false));
      assertThat(page2.hasPrevious(), is(true));
   }

   @Test
   public void testNextPageRequest_page1()
   {
      assertThat(page1.nextPageRequest().isPresent(), is(true));
      assertThat(page1.nextPageRequest().get(), is(request2));
   }

   @Test
   public void testNextPageRequest_page2()
   {
      assertThat(page2.nextPageRequest().isPresent(), is(false));
   }

   @Test
   public void testPreviousPageRequest_page1()
   {
      assertThat(page1.previousPageRequest().isPresent(), is(false));
   }

   @Test
   public void testPreviousPageRequest_page2()
   {
      assertThat(page2.previousPageRequest().isPresent(), is(true));
      assertThat(page2.previousPageRequest().get(), is(request1));
   }

   @Test
   public void testHasContent()
   {
      assertThat(page1.hasContent(), is(true));
   }

   @Test
   public void testHasContent_emptyPage()
   {
      PageRequest<String> request = new PageRequestImpl<>(3, 3);
      Page<String, String> page = new PageImpl<>(new ArrayList<>(), request, 5);
      assertThat(page.hasContent(), is(false));
   }
}
